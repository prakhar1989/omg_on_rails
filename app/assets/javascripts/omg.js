// fix console.log in IE while no in development mode
if(typeof(window.console) != 'object') {
    window.console = { log: function () {} };
}

var target  = "";
var spinner = "";
var spinnerOptions = {
    width: 5,
    length: 25,
    speed: 1,
    color: '#fff'
}

Omg = {
    Layout: {
        emailFormToggler: function() {
            $(".btn-appear1").fadeToggle(0);
            $(".btn-appear2").fadeToggle("slow", "linear");
        },
        emailSubmitToggler: function() {
            $('#email_registration button[type="submit"]').attr('disabled', 'disabled').toggleClass('btn-inverse');
        },
        emailFormCleaner: function() {
            $('#email_registration button[type="submit"]').removeAttr('disabled').removeClass('btn-inverse');
            $('#email_registration .error_list').empty();
        },
        sbToggler: function() {
            $("#pre-login").fadeToggle(1200);
            $("#post-login").fadeToggle(1200, "linear", function() {
                if (1 == $('#post-login:hidden').length) {
                    $('#post-login').empty();
                }

                $('div.progress > div.bar').tooltip('show');

                // # signup tooltip
                window.setTimeout(refreshTooltipPosition, 500);

                // progress bar arrows
                window.setTimeout(refreshArrowsPosition, 500);
            });
        }
    },
    Email: {
        signupHandler: function() {

            // Gets the form data before
            var form_data = {
                first_name: $("input[name='first_name']").val(),
                email: $("input[name='email']").val()
            };

            Omg.Layout.emailSubmitToggler();

            $.ajax({
                url: "/email-signup",
                type: "POST",
                data: form_data,
                cache: false
            }).done(function(response) {
                if ("OK" == response.status) {
                    $('#post-login').append(response.response);
                    FB.XFBML.parse();
                    Omg.Layout.emailFormCleaner();
                    Omg.Layout.sbToggler();
                } else {
                    $('#email_registration').replaceWith(response.response);

                }
            });
        },
        inviteHandler: function() {
            var emails = [];

            // Gets the manual entered email addresses
            if (0 < $('#contact_list').val().length) {
                $.each($('#contact_list').val().split(','), function(index, value) {
                    emails.push({name: null, email: $.trim(value)});
                });
            }

            if (1 > emails.length) {
                alert('Please enter at least one email address.');
                return;
            }

            $.ajax({
                url     : "/email-shipper",
                type    : "POST",
                data    : { recipients: emails },
                cache   : false,
                beforeSend: function(xhr){
                    // don't create new spinner if spinner already exists
                    if(typeof(spinner) != 'object'){
                        target = document.getElementById('spinner');

                        // show it
                        target.style.display = 'block';

                        target.style.top = ($(window).height() / 2 - $(target).height() / 2 ) + "px";
                        target.style.left = ($(window).width() / 2 - $(target).width() / 2 ) + "px";
                        spinner = new Spinner(spinnerOptions).spin(target);
                    }
                },
                success: function(){

                    // stop spinning after success
                    if (typeof(spinner.stop) != 'undefined')
                    {
                        spinner.stop();
                        spinner = "";
                    }

                    // hide spinner box
                    target.style.display = 'none';
                }
            }).done(function(response) {
                if ("OK" == response.status) {

                   // success message after emails are sent
                    alert("You have successfully sent emails to your contact list.");
                } else {

                   // failed to sent message
                    alert("There's a problem sending emails to your contact list :(. Please, try again or contact us if the problem persists: support@ohmygreen.com");
                }
            });
        }
    },
    Facebook: {
        loginHandler: function(response) {

            /*
                TODO: insert loader here
            */

            if (response.status === "connected") {
                $.ajax({
                    url: "/fb-signup",
                    type: "POST",
                    cache: false,
                    beforeSend: function(xhr){
                        // don't create new spinner if spinner already exists
                        if(typeof(spinner) != 'object'){
                            target = document.getElementById('spinner');

                            // show it
                            target.style.display = 'block';

                            target.style.top = ($(window).height() / 2 - $(target).height() / 2 ) + "px";
                            target.style.left = ($(window).width() / 2 - $(target).width() / 2 ) + "px";
                            spinner = new Spinner(spinnerOptions).spin(target);
                        }
                    },
                    success: function(){

                        // stop spinning after success
                        if (typeof(spinner.stop) != 'undefined')
                        {
                            spinner.stop();
                            spinner = "";
                        }

                        // hide spinner box
                        target.style.display = 'none';
                    }
                }).done(function(response) {
                    if ("OK" == response.status) {
                        // Don't use the OMG togger as we don't control when FB events are trigged
                        $('#post-login').empty();
                        $('#post-login').append(response.response);
                        FB.XFBML.parse();
                        $("#pre-login").fadeOut(1200, 'linear');
                        $("#post-login").fadeIn(1200, 'linear', function () {
                            $('.progress > .bar').tooltip('show');

                            // # signup tooltip
                            window.setTimeout(refreshTooltipPosition, 500);

                            // progress bar arrows
                            window.setTimeout(refreshArrowsPosition, 500);
                        });
                    }
                });
            }
        }
    }
};

window.fbAsyncInit = function() {
    FB.init({
        appId                   : app_facebook_app_id,
        channelUrl              : '/channel.html',
        status                  : true,
        cookie                  : true,
        xfbml                   : true,
        frictionlessRequests    : true
    });

    // automagically login when Facebook session is on
    FB.Event.subscribe('auth.authResponseChange', Omg.Facebook.loginHandler);
    FB.getLoginStatus(Omg.Facebook.loginHandler);
};

// Load the SDK Asynchronously
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

/*** Custom Scrip Defs here ***/
String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

/* The js code below runs when the page is loaded. 
 * This is the same code that was on the html page and has
 * been moved here to have better code organization
 */
$(document).ready(function(){

  //Background image initialization
  $.backstretch("/assets/bg/healthy.jpg");

  // fade In effect when page first load
  $("#pre-login").hide().fadeToggle(1200);
 
  //bookmark script
  function bookmark(title, url) {
      if(document.all) { // ie
          window.external.AddFavorite(url, title);
      }
      else if(window.sidebar) { // firefox
          window.sidebar.addPanel(title, url, "");
      }
      else if(window.opera && window.print) { // opera
          var elem = document.createElement('a');
          elem.setAttribute('href',url);
          elem.setAttribute('title',title);
          elem.setAttribute('rel','sidebar');
          elem.click(); // this.title=document.title;
      }
  }

  //Lightbox
  //product images script for lightbox workaround with carousel
  $('#lightBox').lightbox({resizeToFit: false, show: false});

  var imagesLinks = $('a[data-toggle="lightbox"] > img');
  var images      = $('div#lightBox > div.lightbox-content > div.carousel > div.carousel-inner > div.item');
  imagesLinks.click(function (e) {

      // remove all active to carousel
      images.removeClass('active');

      image = $('div#lightBox > div.lightbox-content > div.carousel > div.carousel-inner > div' + $(this).attr('data-target'));
      image.addClass('active');
  });
  //each slide should have a min-height of browser window height

  // $("img.lazy").lazyload({ threshold : 200 });
  $('[rel="popover"]').tooltip();
  $('#navi').localScroll(800);
  RepositionNav();
  $(window).resize(function() {
      RepositionNav();
  });

  // .parallax(xPosition, adjuster, inertia, outerHeight) options:
  // xPosition - Horizontal position of the element.
  // adjuster - y position to start from.
  // inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling.
  // outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport.
  $('#mission').parallax("10%", "50%", 0.4, true);
  //$('#wlecome').parallax("50%", 0, 0.2, true);
  //$('#principles').parallax("30%", 0, 0.4, true);
  $('#story').parallax("50%", 0, 0.4, true);


  $('#navbar').scrollspy();
  $('.odyssey').click(function() {
      $.scrollTo( '#story', 800);
   });
   $('.about').click(function() {
      $.scrollTo( '#welcome', 800);
   });
   $('.signup').click(function() {
      $.scrollTo( '#home', 800);
   });
   
  //$('.thumbnail').popover(['placement', 'top']);
  $(".collapse").collapse()

    
  // Email signup form toggler
  $('#btn-appear').click(function(event) {
      event.preventDefault()
      return Omg.Layout.emailFormToggler();
  });

  // Facebook signup trigger
  $('#fb-login').click(function(event) {
      event.preventDefault();
      return FB.login(Omg.Facebook.loginHandler, { scope: $(this).attr('data-scope') });
  });

  // Email signup form submission trigger
  $(document).delegate('#email_registration button[type="submit"]', 'click', function(event) {
      event.preventDefault();
      return Omg.Email.signupHandler();
  });

  // Logout link
  $(document).delegate('.logout', 'click', function(event) {
      event.preventDefault();
      FB.logout();
      return Omg.Layout.sbToggler();
  });

  // Here's the AJAX call to send emails from the UI
  $(document).delegate('#mailsubmit', 'click', function() {
      event.preventDefault();
      return Omg.Email.inviteHandler();
  });
});
