module ApplicationHelper

  def image_url(source)
    # returns the image_url for an image (asset)
    URI.join(root_url, image_path(source))
  end
end
